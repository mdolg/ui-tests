import allure
import pytest
from selenium.webdriver import ActionChains
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC


@pytest.mark.usefixtures("setup", "log_on_failure")
class BaseClass:

    def scrollToEndPage(self):
        with allure.step("Пролистать до конца страницы"):
            self.driver.execute_script("window.scroll(0,document.body.scrollHeight);")

    def move_to_element(self, *locator):
        with allure.step("Пролистать до определенного элемента"):
            action = ActionChains(self.driver)
            action.move_to_element(self.driver.find_element(*locator)).perform()

    def wait_for_el(self, time, *locator):
        with allure.step("Ожидаем появление элемента"):
            wait = WebDriverWait(self.driver, time)
            return wait.until(EC.visibility_of_element_located(*locator))

