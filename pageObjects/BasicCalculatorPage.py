import allure
from selenium.webdriver.common.by import By

from utilities.BaseClass import BaseClass

from selenium.webdriver.support.select import Select


class BasicCalculatorPage(BaseClass):

    def __init__(self, driver):
        self.driver = driver

    build = (By.CSS_SELECTOR, "select[name='selectBuild']")
    first_number_field = (By.CSS_SELECTOR, "input[id='number1Field']")
    second_number_field = (By.CSS_SELECTOR, "input[id='number2Field']")
    operation = (By.CSS_SELECTOR, "select[id='selectOperationDropdown']")
    calculate_button = (By.CSS_SELECTOR, "input[id='calculateButton']")
    number_answer_field = (By.CSS_SELECTOR, "input[id='numberAnswerField']")
    clear_button = (By.CSS_SELECTOR, "input[id='clearButton']")
    integers_only_checkbox = (By.CSS_SELECTOR, "input[id='integerSelect']")

    def build_drop_down_selection(self, value):
        with allure.step(f"В выпадющем меню Build выбрать значение {value}"):
            return Select(self.driver.find_element(*BasicCalculatorPage.build)).select_by_visible_text(value)

    def fill_first_number_field(self, value):
        with allure.step(f"Заполнить поле First Number значением {value}"):
            return self.driver.find_element(*BasicCalculatorPage.first_number_field).send_keys(value)

    def fill_second_number_field(self, value):
        with allure.step(f"Заполнить поле Second Number значением {value}"):
            return self.driver.find_element(*BasicCalculatorPage.second_number_field).send_keys(value)

    def get_operation(self, value):
        with allure.step(f"Выбрать выпадающее меню операции выбрать {value}"):
            return Select(self.driver.find_element(*BasicCalculatorPage.operation)).select_by_visible_text(value)

    def click_calculate_button(self):
        with allure.step("Нажать кнопку Calculate"):
            return self.driver.find_element(*BasicCalculatorPage.calculate_button).click()

    def get_number_answer_field(self):
        with allure.step("Получение значения в поле Answer"):
            return self.driver.find_element(*BasicCalculatorPage.number_answer_field).get_attribute("value")

    def click_clear_button(self):
        with allure.step("Нажать кнопку Clear"):
            return self.driver.find_element(*BasicCalculatorPage.clear_button).click()

    def click_integer_checkbox(self):
        with allure.step("Нажать чекбокс integers only"):
            return self.driver.find_element(*BasicCalculatorPage.integers_only_checkbox).click()

    def common_steps(self, build, first_input, second_input, operation, url):
        self.driver.get(url)
        self.move_to_element(By.ID, "selectBuild")
        self.build_drop_down_selection(build)
        self.fill_first_number_field(first_input)
        self.fill_second_number_field(second_input)
        self.get_operation(operation)
        self.click_calculate_button()
