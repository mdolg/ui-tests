import allure
import pytest
from selenium.webdriver.common.by import By

from utilities.BaseClass import BaseClass

from selenium.webdriver.support.select import Select


class NumberGamePage(BaseClass):

    def __init__(self, driver):
        self.driver = driver

    build = (By.CSS_SELECTOR, "select[name='buildNumber']")
    roll_dice_button = (By.CSS_SELECTOR, "input[id='rollDiceButton']")
    number_guess_field = (By.CSS_SELECTOR, "input[id='numberGuess']")
    submit_button = (By.CSS_SELECTOR, "input[id='submitButton']")
    alert = (By.XPATH, "//i[contains(text(),'string: Not a number!')]")

    def build_drop_down_selection(self, value):
        with allure.step(f"В выпадющем меню Build выбрать значение {value}"):
            return Select(self.driver.find_element(*NumberGamePage.build)).select_by_visible_text(value)

    def get_roll_dice_button(self):
        with allure.step("Нажать на кнопку Roll the dice"):
            return self.driver.find_element(*NumberGamePage.roll_dice_button).click()

    def get_number_guess_field(self, value):
        with allure.step(f"В поле для угадывания числа ввести число {value}"):
            return self.driver.find_element(*NumberGamePage.number_guess_field).send_keys(value)

    def get_submit_button(self):
        with allure.step("Нажать кнопку Submit"):
            return self.driver.find_element(*NumberGamePage.submit_button).click()

