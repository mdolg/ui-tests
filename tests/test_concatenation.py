import allure
import pytest
from pageObjects.BasicCalculatorPage import BasicCalculatorPage
from utilities.BaseClass import BaseClass
from testData.enums import OperationData
from constants import BASE_URL, CALCULATOR_URL


class TestBasicCalculator(BaseClass):

    @allure.feature("Операции конкатенации")
    @allure.title("Тест конкатенации")
    @allure.description("Конкатенация двух строк")
    @allure.severity(allure.severity_level.CRITICAL)
    @pytest.mark.parametrize("first_input, second_input, result",
                             OperationData.Concatenate.value)
    @pytest.mark.parametrize("build", OperationData.BUILDS.value)
    def test_concatenation(self, first_input, second_input, result, build, url=BASE_URL + CALCULATOR_URL):
        allure_title = f"Тест конкатенации, билд {build}: {first_input} конкатенировать с {second_input} = {result}"
        allure.dynamic.title(allure_title)
        basic_calculator_page = BasicCalculatorPage(self.driver)
        basic_calculator_page.common_steps(build, first_input, second_input, OperationData.Concatenate.name,
                                           url)
        field_to_check = self.wait_for_el(5, basic_calculator_page.number_answer_field)
        with allure.step(
                f"Проверяем, что результат конкатенации, то есть {field_to_check.get_attribute('value')} равняется {result}"):
            assert str(field_to_check.get_attribute("value")) == str(result)
        basic_calculator_page.click_clear_button()
        with allure.step(
                f"Проверяем, что значение в поле пустое "):
            assert field_to_check.get_attribute("value") == ""
