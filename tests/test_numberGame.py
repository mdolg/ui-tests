import allure
import pytest
from pageObjects.NumberGamePage import NumberGamePage
from utilities.BaseClass import BaseClass
from constants import BASE_URL, NUMBER_GAMES_URL


@pytest.mark.flaky(reruns=1)
@allure.epic("Number games")
class TestSecond(BaseClass):
    @allure.title("Тест появления ошибки при введение не числа, а строки")
    @allure.description("Вводим строку вместо числа в поля для угадывания")
    @allure.severity(allure.severity_level.NORMAL)
    def test_NumberGame(self, url=BASE_URL+NUMBER_GAMES_URL):
        number_game_page = NumberGamePage(self.driver)
        self.driver.get(url)
        self.scrollToEndPage()
        number_game_page.build_drop_down_selection("Demo")
        number_game_page.get_roll_dice_button()
        number_game_page.get_number_guess_field("string")
        number_game_page.get_submit_button()
        field_to_check = self.wait_for_el(5, number_game_page.alert)
        assert field_to_check.text == "string: Not a number!1"
