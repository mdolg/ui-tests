import allure
import pytest
from pageObjects.BasicCalculatorPage import BasicCalculatorPage
from utilities.BaseClass import BaseClass
from testData.enums import OperationData
from constants import BASE_URL, CALCULATOR_URL


class TestBasicCalculator(BaseClass):

    @allure.feature("Операции умножения")
    @allure.description("Умножение двух чисел")
    @allure.severity(allure.severity_level.BLOCKER)
    @pytest.mark.parametrize("first_input, second_input, result, result_integer", OperationData.Multiply.value)
    @pytest.mark.parametrize("build", OperationData.BUILDS.value)
    def test_multiplication(self, first_input, second_input, result, result_integer, build,
                            url=BASE_URL + CALCULATOR_URL):
        allure_title = f"Тест умножения, билд {build}: {first_input} умножить на {second_input} = {result}"
        allure.dynamic.title(allure_title)
        basic_calculator_page = BasicCalculatorPage(self.driver)
        basic_calculator_page.common_steps(build, first_input, second_input, OperationData.Multiply.name,
                                           url)
        field_to_check = self.wait_for_el(5, basic_calculator_page.number_answer_field)
        with allure.step(
                f"Проверяем, что результат умножения, то есть {field_to_check.get_attribute('value')} равняется {result}"):
            assert str(field_to_check.get_attribute("value")) == str(result)
        basic_calculator_page.click_integer_checkbox()
        with allure.step(f"Проверяем, что с учетом чек-бокса результат умножения"
                         f" то есть значение {field_to_check.get_attribute('value')} равняется {result_integer}"):
            assert str(field_to_check.get_attribute("value")) == str(result_integer)
        basic_calculator_page.click_clear_button()
        with allure.step(
                f"Проверяем, что значение в поле пустое "):
            assert field_to_check.get_attribute("value") == ""
