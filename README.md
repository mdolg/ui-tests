# Этот проект содержит тесты для страницы https://testsheepnz.github.io/BasicCalculator.html которые проверяют различные комбинации данных для всех "build"(версий) 'Basic Calculator'
## Для запуска тестов:
### Предварительно загрузить образ хрома(после загрузки изменить данные на скачанный образ в файлах conftest.py и browsers.json, текущий образ это образ для mac arm64) и выполнить команду "docker-compose up -d" что бы поднять контейнеры с селеноидом и аллюром
### Выполнить команду "pytest -vs --alluredir=allure/allure-results tests/ -n 5" для запуска тестов